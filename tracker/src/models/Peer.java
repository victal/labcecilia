package models;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import server.Tracker;
import utils.XML;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;

public class Peer {

	private String ID;
	private String IP;
	private int port;
	
	private static final String HOME = Tracker.HOME + "peers/";
	

	public Peer(String id, String ip, int port) {
		this.ID = id;
		this.IP = ip;
		this.port = port;
	}

	public String getID() {
		return ID;
	}

	public String getIP() {
		return IP;
	}

	public int getPort() {
		return port;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public void setIP(String iP) {
		IP = iP;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
	
	/*
	 * Where does this object lives ...
	 * Fetch complete filepath by ID
	 */
	private static String getFilePath(String id) {
		String path = HOME + id;
		return path;
	}

	/*
	 * Just a helper function to public DB get method
	 */
	private static Peer _get(File file) {
		XStream xstream = XML.getXStream();
		try {
			Peer ret = (Peer) xstream.fromXML(file);
			return ret;
		} catch (XStreamException e) {
			e.printStackTrace();
			return null;
		}
	}

	/*
	 * DB: Given an ID, get the object 
	 * 
	 */
	public static Peer get(String id) {
		File file = new File(getFilePath(id));
		if (!file.exists()) return null;
		return _get(file);
	}
	
	/*
	 * DB: Persists this object into a XML file for further retrieval 
	 */
	public void save() {
		File out = new File(getFilePath(this.ID));
		String xml = this.toXML();
		try {
			FileUtils.writeStringToFile(out, xml);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/*
	 * Get an XML plain representation of this object 
	 */
	public String toXML() {
		return XML.toXML(this);
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ID == null) ? 0 : ID.hashCode());
		result = prime * result + ((IP == null) ? 0 : IP.hashCode());
		result = prime * result + port;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Peer other = (Peer) obj;
		if (ID == null) {
			if (other.ID != null)
				return false;
		} else if (!ID.equals(other.ID))
			return false;
		if (IP == null) {
			if (other.IP != null)
				return false;
		} else if (!IP.equals(other.IP))
			return false;
		if (port != other.port)
			return false;
		return true;
	}
	

}
