package models;

import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import server.PeerTask;
import server.PieceTask;
import server.TrackerFileTask;
import utils.ResponseUtils;
import utils.ResponseUtils.InvalidResourceHandler;



public class Resource {
	
	public static final String INVALID_RESOURCE = "Invalid resource";
	
	public static final String TRACKER_FILE_LIST_RESOURCE = "";
	public static final String TRACKER_FILE_SUFFIX = ".tracker";
	public static final String TRACKER_FILE_TYPE = "TRACKER_FILE_TYPE";

	public static final String PEER_TYPE = "PEER_TYPE";
	public static final String PIECE_TYPE = "PIECE_TYPE";
	
	private String uri;
	private boolean isValid = false;
	
	private Map<String, String> info = new HashMap<String, String>(); 
	
	public Resource(String uri) {
		this.uri = uri;
		
		isValid = parseTrackerFileResource(uri);
		if (isValid) return;
				
		isValid = parsePeerResource(uri);
		if (isValid) return;
		
		isValid = parsePieceResource(uri);
		if (isValid) return;
				
		System.out.println("[LOG]: Parsed route to an Invalid resource");
	}

	/*
	 *  curl -X PUT  localhost:8080/resource.tracker/pieces/2/peers/123
	 */
	private boolean parsePieceResource(String uri) {

		// trying to find resource name 
		uri = uri.substring(1);
		int index1 = uri.indexOf('/');
		if (!uri.substring(0, index1).endsWith(TRACKER_FILE_SUFFIX)) return false;
		String trackerfile = uri.substring(0, uri.indexOf(".tracker"));
		
		// trying to find "pieces" string
		uri = uri.substring(index1 + 1);
		index1 = uri.indexOf('/');
		if (!uri.substring(0, index1).equals("pieces")) return false;

		// trying to find pieceId
		uri = uri.substring(index1 + 1);
		index1 = uri.indexOf('/');
		String pieceId = uri.substring(0, index1);
		
		// trying to find "peers" string
		uri = uri.substring(index1 + 1);
		index1 = uri.indexOf('/');
		if (!uri.substring(0, index1).equals("peers")) return false;
		
		// trying to find peerId
		uri = uri.substring(index1 + 1);
		String peerId = uri;
		
		// Verify if peerId and pieceId really have numbers 
		try {
			Integer.parseInt(pieceId);
			Integer.parseInt(peerId);
		} catch (NumberFormatException e) {
			return false;
		}

		this.info.put("peerId", peerId);
		this.info.put("pieceId", pieceId);
		this.info.put("file", trackerfile);
		this.info.put("type", PIECE_TYPE);
		return true;
	}

	/*
	 * Peer is requesting an update of its ip:port on the tracker server:
	 * curl -X PUT -d "ip=127.0.0.1&port=8080" localhost:8080/peers/123
	 */
	private boolean parsePeerResource(String uri) {
		
		int index1 = uri.indexOf('/');
		int index2 = uri.lastIndexOf('/');

		if (!uri.substring(index1, index2).equals("/peers")) return false;
		
		String peerId = uri.substring(index2 + 1);

		try {
			int i = Integer.parseInt(peerId);
		} catch (NumberFormatException e) {
			return false;
		}
		
		System.out.println("[LOG]: Parsed route to a peer resource");
		this.info.put("peerId", peerId);
		this.info.put("type", PEER_TYPE);
		return true;
	}


	/*
	 * Announce a new tracker file to the tracker server:
	 * curl -X POST -d "name=Name&length=4096&description=Desc&checksum=42&checksum_piece1=42&peer_id=1" localhost:8080/
	 * 
	 * Retrieve list of tracker files available on this server:
	 * curl -X GET localhost:8080/ 
	 * 
	 * Retrive this specific tracker file:
	 * curl -X GET localhost:8080/Name.tracker
	 * 
	 */
	private boolean parseTrackerFileResource(String uri) {
		if (uri.equals("/")) {
			this.info.put("file", TRACKER_FILE_LIST_RESOURCE);
			this.info.put("type", TRACKER_FILE_TYPE);
			return true;
		}
		if (!uri.endsWith(TRACKER_FILE_SUFFIX)) return false;
		
		String file = uri.substring(1);
		file = file.substring(0, file.lastIndexOf(".tracker"));

		System.out.println("[LOG]: Parsed route to a trackerfile resource");
		this.info.put("file", file);
		this.info.put("type", TRACKER_FILE_TYPE);
		return true;
	}

	public String getFileName() {
		return this.info.get("file");
	}
	

	public String getPeerId() {
		return this.info.get("peerId");
	}
	
	public String getPieceId() {
		return this.info.get("pieceId");
	}
	
	public String getType() {
		return this.info.get("type");
	}

	public Runnable POST(Request request, Response response) {
		if (getType().equals(Resource.INVALID_RESOURCE))	return new ResponseUtils.InvalidResourceHandler(request, response);
		else if (getType().equals(TRACKER_FILE_TYPE)) return new TrackerFileTask.POST(request, response);

		return new ResponseUtils.InvalidResourceHandler(request, response);
	}
	
	public Runnable PUT(Request request, Response response) {
		if (getType().equals(Resource.INVALID_RESOURCE))	return new ResponseUtils.InvalidResourceHandler(request, response);
		else if (getType().equals(Resource.PEER_TYPE)) return new PeerTask.PUT(request, response);
		else if (getType().equals(Resource.PIECE_TYPE)) return new PieceTask.PUT(request, response);
		
		return new ResponseUtils.InvalidResourceHandler(request, response);
	}


	public Runnable GET(Request request, Response response) {
		if (getType().equals(Resource.INVALID_RESOURCE))	return new ResponseUtils.InvalidResourceHandler(request, response);
		else if (getType().equals(TRACKER_FILE_TYPE)) return new TrackerFileTask.GET(request, response);
		
		return new ResponseUtils.InvalidResourceHandler(request, response);
	}


	
	
	
	

}
