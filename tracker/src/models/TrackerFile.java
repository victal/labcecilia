package models;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import server.Tracker;
import utils.XML;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;


public class TrackerFile {

	private Integer id;
	private String name;
	private String description;
	private Integer length;
	private List<Piece> pieces;
	private Integer numpieces;
	private String checksum;

	public static final Integer PIECESIZE = 4096;  // bytes

	private static final String LIST = Tracker.HOME + "list";


	public TrackerFile(String name, String description, Integer length, String checksum, List<String> piecesChecksum, Peer announcer) {
		this.name = name;
		this.description = description;
		this.length = length;
		this.pieces = new ArrayList<Piece>();

		this.numpieces = 0;
		for (String c : piecesChecksum) {
			int pieceLength = Math.min(PIECESIZE, length);
			this.numpieces++;
			Piece p = new Piece(numpieces, pieceLength , Arrays.asList(announcer));
			p.setChecksum(c);
			this.pieces.add(p);
			length -= PIECESIZE;
		}

		this.checksum = checksum;

		this.id = name.hashCode();
		// TODO
		//this.checksum = 42;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNumpieces() {
		return numpieces;
	}


	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	private List<Piece> getPieces() {
		return this.pieces;
	}


	/*
	 * Where does this object live ...
	 * Fetch complete filepath by ID
	 */
	private static String getFilePath(Integer id) {
		String path = Tracker.HOME + id;
		return path;
	}

	/*
	 * Where does this object lives ...
	 * Fetch complete filepath by Name
	 */
	private static String getFilePath(String name) {
		String path = Tracker.HOME + name.hashCode();
		return path;
	}

	/*
	 * Just a helper function to public DB get method
	 */
	private static TrackerFile _get(File file) {
		XStream xstream = XML.getXStream();
		try {
			TrackerFile ret = (TrackerFile) xstream.fromXML(file);
			updateHealth(ret);
			return ret;
		} catch (XStreamException e) {
			e.printStackTrace();
			return null;
		}
	}

	/*
	 * DB: Given an ID, get the object
	 *
	 */
	public static TrackerFile get(Integer id) {
		File file = new File(getFilePath(id));
		if (!file.exists()) return null;
		return _get(file);
	}

	/*
	 * DB: Given a Name, get the object
	 *
	 */
	public static TrackerFile get(String name) {
		File file = new File(getFilePath(name.hashCode()));
		if (!file.exists()) return null;
		return _get(file);
	}

	/*
	 * DB: Announce (create) a new Tracker file
	 */
	public static TrackerFile announce(String name, String description, Integer length, String checksum, List<String> piecesChecksum, Peer annoucer) {
		TrackerFile tf = new TrackerFile(name, description, length, checksum, piecesChecksum, annoucer);
		if (tf.getNumpieces() != tf.getPieces().size()) {
			System.out.println("[LOG]:" + "Invalid parameter to announce Tracker File " + name);
			return null;
		}
		tf.save();
		return tf;
	}



	/*
	 * Get an XML plain representation of this object
	 */
	public String toXML() {
		return XML.toXML(this);
	}

	/*
	 * sync the pieces::peers list with the actual information in the peer DB
	 * (much like a typical SQL JOIN between TrackerFile table and Piece table)
	 */
	private static void updateHealth(TrackerFile trackerFile) {
		for (Piece p : trackerFile.pieces) {
			p.updateHealth();
		}
	}


	/*
	 * DB: Persists this object into a XML file for further retrieval
	 */
	public void save() {
		File out = new File(getFilePath(this.id));
		String xml = this.toXML();
		try {
			FileUtils.writeStringToFile(out, xml);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/*
	 * DB: List all tracker files resources in the tracker server
	 */
	public static List<TrackerFile> getAll() {
		Collection<File> files = FileUtils.listFiles(new File(Tracker.HOME), null, false);
		List<TrackerFile> trackerFiles = new ArrayList<TrackerFile>();
		for (File f : files) {
			trackerFiles.add(get((Integer.valueOf(f.getName()))));
		}
		return trackerFiles;
	}



	/*
	 * A seed is a peer with every possible piece complete
	 */
	public void addSeed(Peer seed) {
		for (int i = 0; i < this.numpieces; i++) {
			Piece p = this.pieces.get(i);
			p.addPeer(seed);
		}
	}


	/*
	 * Just a helper method to help testing and emulating
	 * delete me (later)
	 *
	 */
	public static TrackerFile buildDummyTF(String resourceName) {
		// announce the tracker file with two dumb seeds
		Peer seedA = new Peer("1", "127.0.0.1", 8080);
		seedA.save();
		Peer seedB = new Peer("2", "127.0.0.1", 8000);
		seedB.save();

		TrackerFile tf = TrackerFile.announce(resourceName, "some description", 5000, "42", Arrays.asList("a", "b"), seedA);
		tf.addSeed(seedB);

		tf.save();
		return tf;
	}

	public boolean hasPiece(String pieceId) {
		return (this.pieces.size() >= Integer.parseInt(pieceId));
	}

	public void addPiece(Peer peer, String pieceId) {
		this.pieces.get(Integer.parseInt(pieceId) - 1).addPeer(peer);
	}




}
