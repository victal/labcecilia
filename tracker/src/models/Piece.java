package models;

import java.util.ArrayList;
import java.util.List;

public class Piece {


	public Piece(Integer id, Integer size, List<Peer> peers) {
		this.id = id;
		this.size = size;
		this.peerList = new ArrayList<Peer>();
		for (Peer peer : peers) {
			this.peerList.add(peer);
		}

		// TODO
		this.check_sum = "42";
	}

	private Integer id;
	private Integer size;
	private String check_sum;
	private List<Peer> peerList;


	public void addPeer(Peer p) {
		if (!this.peerList.contains(p))
			this.peerList.add(p);
	}


	public String getChecksum() {
		return check_sum;
	}


	public void setChecksum(String checksum) {
		this.check_sum = checksum;
	}


	/*
	 * Update the peer list location to correct, most updated ip:port
	 * (much like a typical SQL JOIN between Piece table and Peer table)
	 */
	public void updateHealth() {
		for (Peer p : peerList) {
			Peer updatedPeer = Peer.get(p.getID()); // fetch from "DB"
			if (!updatedPeer.equals(p)) {
				p.setIP(updatedPeer.getIP());
				p.setPort(updatedPeer.getPort());
				//this.peerList.set(i, currentPeer);
			}
		}
	}


}
