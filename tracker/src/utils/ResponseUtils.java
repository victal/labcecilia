package utils;

import java.io.IOException;
import java.io.PrintStream;

import models.Resource;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;



public class ResponseUtils  {
	
	public static class InvalidResourceHandler implements Runnable {
		private final Response response;
		private final Request request;
		
		public InvalidResourceHandler(Request request, Response response) {
			this.response = response;
			this.request = request;
		}


		public void run() {
			System.out.println("[LOG]:" + "Invalid resource");
			PrintStream body = null;
			try {
				body = response.getPrintStream();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			setDefaultResponseHeader(response);
			response.setCode(404);
			
			body.println(Resource.INVALID_RESOURCE);
			
			body.close();
		}
	}


	/*
	 * HTTP Response should have proper headers
	 */
	public static void setDefaultResponseHeader(Response response) {
		long time = System.currentTimeMillis();
		response.set("Content-Type", "text/plain");
		response.set("Server", "The Amazing Tracker Server/1.0 (ITA)");
		response.setDate("Date", time);
		response.setDate("Last-Modified", time);
	}	

}
