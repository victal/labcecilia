package utils;

import models.Peer;
import models.Piece;
import models.TrackerFile;

import com.thoughtworks.xstream.XStream;

public class XML {



	public static XStream getXStream() {
		XStream xstream = new XStream();
		xstream.setMode(XStream.NO_REFERENCES);
		xstream.alias("file", TrackerFile.class);
		xstream.alias("peer", Peer.class);
		xstream.alias("piece", Piece.class);
		return xstream;
	}

	public static String toXML(Object obj) {
		XStream xstream = getXStream();
		String xml = xstream.toXML(obj);
//		System.out.println("[LOG]: " + xml);
		return xml;
	}

}
