package server;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;

import models.Resource;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.simpleframework.http.core.Container;
import org.simpleframework.transport.connect.Connection;
import org.simpleframework.transport.connect.SocketConnection;
import org.simpleframework.util.thread.Scheduler;

import server.TrackerFileTask.GET;
import server.TrackerFileTask.POST;

public class Tracker implements Container {
	
	public static final String HOME = "/tmp/tracker/";
	
	private Scheduler queue;
	
	public Tracker(Scheduler queue) {
	      this.queue = queue;
	 }
	
	public void handle(Request request, Response response) {
		String method = request.getMethod();  // "GET" or "POST" or "PUT"
		System.out.println("[LOG]:" + method);
		
		/*
		* Retrieve list of tracker files available on this server:
		* curl -X GET localhost:8080/ 
		* 
		* Retrive this specific tracker file:
		* curl -X GET localhost:8080/Name.tracker
		* 
		*/
		if (method.equals("GET")) {
 			Resource resource = new Resource(request.getPath().getPath());
			this.queue.execute(resource.GET(request, response));
			
		}
		
		/*
		 * 	Announce a new tracker file (creation)
		 *  curl -X POST -d "name=Name&length=4096&description=Desc&checksum=42&checksum_piece1=42&peer_id=1" localhost:8080/
		 *  Make sure the peer exists before (i.e. it is registered with the tracker server)
		 */
		if (method.equals("POST")) {
			Resource resource = new Resource(request.getPath().getPath());
			this.queue.execute(resource.POST(request, response));
		}
		
		/*
		 * Peer is requesting an update of its ip:port on the tracker server (or a create action):
		 * curl -X PUT -d "ip=127.0.0.1&port=8080" localhost:8080/peers/123
		 */
		/*
		 * Peer wants to let tracker know he has just finished downloading of some piece
		 * curl -X PUT  /resource.tracker/pieces/2/peers/123 
		 */
		if (method.equals("PUT")) {
			Resource resource = new Resource(request.getPath().getPath());
			this.queue.execute(resource.PUT(request, response));
		}
					
	}

	
	public static void main(String[] args) throws Exception {
		(new File(HOME)).mkdirs();
		
		Scheduler scheduler = new Scheduler(40);
		Container container = new Tracker(scheduler);
		Connection connection = new SocketConnection(container);
		SocketAddress address = new InetSocketAddress(8080);

		connection.connect(address);
	}
	

	
}
