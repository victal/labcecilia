package server;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import models.Peer;
import models.Resource;
import models.TrackerFile;

import org.simpleframework.http.Form;
import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.simpleframework.http.parse.AddressParser;

import utils.ResponseUtils;

public class TrackerFileTask {

	public static final String ROOTURI = "/";

	/*
	* Retrieve list of tracker files available on this server:
	* curl -X GET localhost:8080/
	*
	* Retrive this specific tracker file:
	* curl -X GET localhost:8080/Name.tracker
	*
	*/
	public static class GET implements Runnable {
		private final Response response;
		private final Request request;
		private String uri;
		private Resource resource;

		public GET(Request request, Response response) {
			this.response = response;
			this.request = request;
			this.uri = request.getPath().getPath();
			this.resource = new Resource(uri);
		}


		public void run() {
			System.out.println("[LOG]:" + this.uri);
			PrintStream body = null;
			try {
				body = response.getPrintStream();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			ResponseUtils.setDefaultResponseHeader(response);

			String bodyContents = getBodyContents(resource.getFileName());
			System.out.println("[LOG]:" + bodyContents);
			body.println(bodyContents);

			body.close();
		}


		/*
		 * Based on the required resource on the request, build the proper body content
		 */
		private String getBodyContents(String resourceName) {
			StringBuilder sb = new StringBuilder();

			if (resourceName.equals(Resource.TRACKER_FILE_LIST_RESOURCE)) {
				List<TrackerFile> files = TrackerFile.getAll();
				for (TrackerFile tf : files) {
					sb.append(tf.getName() + Resource.TRACKER_FILE_SUFFIX);
					sb.append("\t");
					sb.append(tf.getDescription());
					sb.append("\n");
				}
			}
			else {
				TrackerFile t = TrackerFile.get(resourceName);
				if (t == null) sb.append(Resource.INVALID_RESOURCE);
				else sb.append(t.toXML());
			}
			return sb.toString();
		}


	}



	/*
	 * 	Announce a new tracker file (creation)
	 *  curl -X POST -d "name=Name&length=4096&description=Desc&checksum=42&checksum_piece1=42&peer_id=1" localhost:8080/
	 */
	public static class POST implements Runnable {
		private final Response response;
		private final Request request;
		private String uri;
		private String resourceName;

		public POST(Request request, Response response) {
			this.response = response;
			this.request = request;
			this.uri = request.getPath().getPath();
		}

		public void run() {
			PrintStream body = null;
			try {
				body = response.getPrintStream();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Map<String,String> data = null;
			try {
				data = request.getForm();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			ResponseUtils.setDefaultResponseHeader(response);

			List<String> piecesChecksum = parsePiecesChecksum(data);
			Peer announcer = Peer.get(data.get("peer_id"));

			TrackerFile tf = TrackerFile.announce(data.get("name"), data.get("description"), Integer.parseInt(data.get("length")), data.get("checksum"), piecesChecksum, announcer);
			if (tf != null) {
				response.setCode(201);
				//System.out.println("[LOG]: " + tf.toXML());
			} else {
				body.println("Impossible to create new Tracker. Check your parameters");
			}
			body.close();
		}



		private List<String> parsePiecesChecksum(Map<String,String> data) {
			String prefix = "checksum_piece";

			int numpieces= 0;
			for (String k : data.keySet())
				if (k.startsWith(prefix)) numpieces++;


			String[] _temp = new String[numpieces];
			List<String> ret = new ArrayList<String>();
			for (String k: data.keySet()) {
				if (k.startsWith(prefix)) {
					Integer pieceId = Integer.parseInt(k.substring(prefix.length()));
					_temp[pieceId - 1] = data.get(k);
				}
			}

			for (int i = 0; i < numpieces; i++) {
				ret.add(_temp[i]);
			}

			return ret;
		}

	}



}
