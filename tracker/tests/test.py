#!/usr/bin/python
"""
SUPER UGLY POOR MAN TESTING SCRIPT

TRACKER SERVER
"""

import commands
import os


def run(cmd):
    (status, output) = commands.getstatusoutput(cmd)
    return (status, output)

# clean up everything
run("rm -Rf /tmp/trackers")
run("mkdir /tmp/trackers")

# create some peers
run('curl -X PUT -d "ip=127.0.0.1&port=8080" localhost:8080/peers/1')
run('curl -X PUT -d "ip=127.0.1.1&port=8080" localhost:8080/peers/2')

# peer1 is announcing a new file
run('curl -X POST -d "name=Name&length=4096&description=Desc&checksum=42&checksum_piece1=42&peer_id=1" localhost:8080/')

# TEST: peer1 announce correctly
run('curl localhost:8080/ > trackerlist')
run('curl localhost:8080/Name.tracker > name.tracker')
status = run('diff correct/trackerlist trackerlist')[0]
status = status and run('diff correct/name.tracker name.tracker')[0]
if (status != 0):
    print 'ERROR: Peer1 is not announcing correctly'
    #print output
else:
    print 'OK: Peer1 is announcing correctly'

# update peer1 ip:port settings
run('curl -X PUT -d "ip=100.0.0.1&port=8000" localhost:8080/peers/1')


#TEST: name.tracker should have correct peer1
run('curl localhost:8080/Name.tracker > name.tracker.afterpeer1change')
status = run('diff correct/name.tracker.afterpeer1change name.tracker.afterpeer1change')[0]
if (status != 0):
    print 'ERROR: Cant fetch file tracker correctly after peer1 settings changes'
#    print output
else:
    print 'OK: File tracker is being updated by any changes to peer1 settings'


# Peer2 says it also has the piece1 of Name.tracker
run('curl -X PUT localhost:8080/Name.tracker/pieces/1/peers/2')
run('curl localhost:8080/Name.tracker > name.tracker.afterpeer2')
(status, output) = run('diff correct/name.tracker.afterpeer2 name.tracker.afterpeer2')
if (status != 0):
    print 'ERROR: Cant make peer2 a owner of piece1. name.tracker updated incorrectly'
    print output
else:
    print 'OK: peer2 is also a owner of piece1. name.tracker updated correctly'




# era everything created by this test script
run('rm name.tracker name.tracker.afterpeer1change trackerlist name.tracker.afterpeer2')

