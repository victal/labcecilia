package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Properties;

import upload.UploadServer;
import utils.TrackerUtils;
import utils.Utils;
import download.Downloader;
import download.SharedFile;

public class Main {

	public static final int DEFAULT_SERVER_PORT = 6626;
	public static final String DEFAULT_DOWNLOAD_FOLDER = System.getProperty("user.home")+File.separator+"Downloads";
	public static final String DEFAULT_CONFIG_FILE = System.getProperty("user.home")+File.separator+".peer_config";
	public static final String DEFAULT_TRACKERFILE_FOLDER = System.getProperty("user.home")+File.separator+".tracker_files";
	public static final Properties properties = new Properties();
	public static void main(String[] args) throws IOException, InterruptedException {


		String trackerFile = null;
		loadProperties();
		if(args.length == 0 || args.length > 4){
			printUsage();
			System.exit(1);
		}
		boolean noport = false;
		if(args.length >=2){
			if(args[0].trim().equals("generate")){
				SharedFile done = generateFile(args[1]);
				if(done!=null){
					System.out.println("Generation of .tracker for "+args[1]+" complete");
					if(args.length >= 3){
						System.out.println("Sending .tracker info to tracker");
						String desc = "No Description";
						if(args.length==4)desc = args[3];
						TrackerUtils.sendAlive(args[2]);
						TrackerUtils.createFileTracker(done,args[2],desc);
					}
					System.out.println("Exiting.");
					System.exit(0);
				}

			}
			if(args[0].trim().equals("list")){
				System.out.println("List of shared files on "+args[1]);
				TrackerUtils.getFileList(args[1]);
				System.exit(0);
			}

			int port = DEFAULT_SERVER_PORT;
			try {
				port = Integer.parseInt(args[1]);
			} catch (NumberFormatException e) {
//				e.printStackTrace();
				//can be .tracker
				noport = true;
			}
			if(port>65535 || port <=0){
				System.out.println("Invalid port, defaulting to "+properties.getProperty("peer_port"));
			}
			else if(!noport){
				properties.setProperty("peer_port", args[1]);
			}
		}
		properties.setProperty("tracker", args[0]);
        //Can't use tracker? -> comment all TrackerUtils.*(Main.TRACKER,.*);
		TrackerUtils.sendAlive(properties.getProperty("tracker"));
		System.out.println("Loading files available for upload...");
		SharedFile.loadSharedFiles();
		System.out.println("Starting Upload Server");
		UploadServer us = new UploadServer();
		Thread server = new Thread(us);
		server.start();
		if(args.length == 3){
			trackerFile = args[2];
		}else if(args.length == 2 && noport){
			trackerFile = args[1];
		}
		if(trackerFile!=null){
			File f = new File(trackerFile);
			if(!f.exists()){
				System.out.println("Tracker file not found. Trying to retrieve from tracker server...");
				f = TrackerUtils.retrieveTrackerFile(properties.getProperty("tracker"),trackerFile);
				BufferedReader br = new BufferedReader(new FileReader(f));
				String line = br.readLine();
				br.close();
					if(line.startsWith("Invalid resource")){
						f.delete();
						f= null;
					}
					else trackerFile = f.getAbsolutePath();
				}
			if(f!=null){
				SharedFile dsf = TrackerUtils.parse(f);
				System.out.println("Parsed .tracker file");
				Downloader dl = new Downloader(dsf);
				Thread download = new Thread(dl);
				download.start();
				download.join(); // for now we do nothing while downloading
				if(dsf.isWhole()){
					System.out.println("Download of "+dsf.getName()+" complete.");
					File tff = new File(properties.getProperty("trackerfile_folder"));
					if(!tff.exists())
						tff.mkdir();
					System.out.println(trackerFile);
					Path src = Paths.get(trackerFile);
					String[] split = trackerFile.split("\\"+File.separator);
					String file = split[split.length-1];
					Path target = Paths.get(properties.getProperty("trackerfile_folder"), file);
					try {
						Files.move(src, target,StandardCopyOption.REPLACE_EXISTING);
						System.out.println("Tracker file copied to upload directory.");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			else{
				System.out.println("Could not find the correct tracker file. Running as Uploader");
			}
		}

	}

	public static SharedFile generateFile(String file) {
		File f = new File(file);
			if(!f.exists()){
				System.out.println("Could not find "+file);
				return null;
			}
			else{
				try{
				SharedFile sf = SharedFile.makeNewSharedFile(file);
				String xml = TrackerUtils.save(sf);
				String[] split = file.split("\\"+File.separator);
				String tfile = properties.getProperty("trackerfile_folder")+File.separator+split[split.length-1]+".tracker";
				File tf = new File(tfile);
				BufferedWriter bw = new BufferedWriter(new FileWriter(tf));
				bw.write(xml);
				bw.close();
				System.out.println("Tracker file created.");
				Path src = Paths.get(file);
				Path ybr = Paths.get(properties.getProperty("share_folder"),split[split.length-1]);
				Files.copy(src,ybr,StandardCopyOption.REPLACE_EXISTING);
				System.out.println("Copied file to upload folder.");
				return sf;
				}catch(IOException e){
					e.printStackTrace();
					return null;
				}
			}
	}
	private static void loadProperties() {
		try{
			BufferedReader br = new BufferedReader(new FileReader(DEFAULT_CONFIG_FILE));
			properties.load(br);
			br.close();
		}catch(IOException e){
			//Do nothing for now
		}
		if(!properties.stringPropertyNames().contains("peer_port")){
			properties.setProperty("peer_port", new Integer(DEFAULT_SERVER_PORT).toString());
		}
		if(!properties.stringPropertyNames().contains("share_folder")){
			properties.setProperty("share_folder", DEFAULT_DOWNLOAD_FOLDER);
		}
		if(!properties.stringPropertyNames().contains("trackerfile_folder")){
			properties.setProperty("trackerfile_folder", DEFAULT_TRACKERFILE_FOLDER);
		}
		File f = new File(properties.getProperty("share_folder"));
		if(!f.exists()){
			System.out.print("Folder "+properties.getProperty("share_folder")+" not found, defaulting to ");
			System.out.println(DEFAULT_DOWNLOAD_FOLDER);
			File ff = new File(DEFAULT_DOWNLOAD_FOLDER);
			if(!ff.exists()){
				System.out.println("Directory "+DEFAULT_DOWNLOAD_FOLDER+" not found. Creating...");
				ff.mkdir();

			}
		}
		if(!properties.stringPropertyNames().contains("peer_id")){
			System.out.println("Generating new Peer ID");
			properties.setProperty("peer_id", Utils.getNewPeerID());
		}
		try{
			BufferedWriter out = new BufferedWriter(new FileWriter(new File(DEFAULT_CONFIG_FILE)));
			properties.store(out, "");
			System.out.println("properties saved.");
		}catch (IOException e){

		}
	}

	private static void printUsage(){
		//System.out.println("o endereço do tracker e a porta devem ser passados como argumentos");
		System.out.println("Usage:");

		System.out.println("To upload files:");
		System.out.println("java -jar LabCecilia.jar <tracker:port> [port]");

		System.out.println("To download and upload files:");
		System.out.println("java -jar LabCecilia.jar <tracker:port> [port] <path to .tracker file>");
		System.out.println("or");
		System.out.println("java -jar LabCecilia.jar <tracker:port> [port] <file name>");

		System.out.println("To generate tracker files: \njava -jar LabCecilia.jar generate <file> [http://tracker:tracker port] [description]");
		System.out.println("tracker file will be generated at "+properties.getProperty("trackerfile_folder")+" and file fill be copied to "+properties.getProperty("share_folder"));

		System.out.println("To list current shared files on tracker:");
		System.out.println("java -jar LabCecilia.jar list <tracker:port>");
	}

}
