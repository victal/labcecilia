package main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import upload.UploadServer;
import utils.TrackerUtils;
import utils.Utils;
import download.SharedFile;

public class UploaderTestMain {

	public static String peer_id = "";
	public static final int DEFAULT_SERVER_PORT = 6626;
	public static int server_port = DEFAULT_SERVER_PORT;
	public static final String DEFAULT_DOWNLOAD_FOLDER = System.getProperty("user.home")+File.separator+"Downloads";
	public static void main(String[] args) {
		
		int port = DEFAULT_SERVER_PORT;
		server_port = port;
		peer_id = Utils.getNewPeerID();
		
SharedFile sh = SharedFile.makeNewSharedFile(DEFAULT_DOWNLOAD_FOLDER+"/teste.txt");
		
		File testfile = new File(System.getProperty("user.home")+"/.tracker_files/teste.txt.tracker");
		try{
			BufferedWriter w = new BufferedWriter(new FileWriter(testfile));
			w.write(TrackerUtils.save(sh));
			w.close();
		}catch(IOException e){
			e.printStackTrace();
		}	
		SharedFile sf = TrackerUtils.parse(testfile);
		
		
		sh = SharedFile.makeNewSharedFile(DEFAULT_DOWNLOAD_FOLDER+"/teste2.txt");
		testfile = new File(System.getProperty("user.home")+"/.tracker_files/teste2.txt.tracker");
		try{
			BufferedWriter w = new BufferedWriter(new FileWriter(testfile));
			w.write(TrackerUtils.save(sh));
			w.close();
		}catch(IOException e){
			e.printStackTrace();
		}	
		sf = TrackerUtils.parse(testfile);
		//String server = args[0];
		//sendAlive(tracker);
		SharedFile.loadSharedFiles();
		System.out.println("Starting Upload Server");
		UploadServer us = new UploadServer();
		Thread server = new Thread(us);
		server.start();

	}


}
