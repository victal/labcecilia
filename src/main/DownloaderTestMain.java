package main;

import java.io.File;

import utils.TrackerUtils;
import utils.Utils;
import download.Downloader;
import download.SharedFile;

public class DownloaderTestMain {

	public static String peer_id = "";
	public static final int DEFAULT_SERVER_PORT = 6626;
	public static int server_port = DEFAULT_SERVER_PORT;
	public static final String DEFAULT_DOWNLOAD_FOLDER = System.getProperty("user.home")+File.separator+"Downloads";
	public static final String DEFAULT_TRACKERFILE_FOLDER = System.getProperty("user.home")+File.separator+".tracker_files";
	public static void main(String[] args) {
		
		int port = DEFAULT_SERVER_PORT;
		peer_id = Utils.getNewPeerID();
		server_port = port;
		File testfile = new File(System.getProperty("user.home")+"/.tracker_files/teste.txt.tracker");
		SharedFile sf = TrackerUtils.parse(testfile);
		sf.setFullPath(DEFAULT_DOWNLOAD_FOLDER+File.separator+sf.getName());
		Downloader d = new Downloader(sf);
		d.run();
		System.out.println(sf.calculateMD5Sum());
		System.out.println(sf.getOriginalMD5());
		System.out.println(sf.calculateMD5Sum().equals(sf.getOriginalMD5())); 
		System.out.println("done");
		
		testfile = new File(System.getProperty("user.home")+"/.tracker_files/teste2.txt.tracker");
		sf = TrackerUtils.parse(testfile);
		sf.setFullPath(DEFAULT_DOWNLOAD_FOLDER+File.separator+sf.getName());
		d = new Downloader(sf);
		d.run();
		System.out.println(sf.calculateMD5Sum());
		System.out.println(sf.getOriginalMD5());
		System.out.println(sf.calculateMD5Sum().equals(sf.getOriginalMD5())); 
		System.out.println("done");
	
		
	}


}
