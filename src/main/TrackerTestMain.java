package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import utils.TrackerUtils;
import utils.Utils;
import download.SharedFile;

public class TrackerTestMain {

	public static final int DEFAULT_SERVER_PORT = 6626;
	public static final String DEFAULT_DOWNLOAD_FOLDER = System.getProperty("user.home")+File.separator+"Downloads";
	public static final String DEFAULT_CONFIG_FILE = System.getProperty("user.home")+File.separator+".peer_config";
	public static final Properties properties = new Properties();
	public static void main(String[] args) throws IOException {
		
		loadProperties();
		System.out.println(Utils.getLocalIP());
		System.out.println(Utils.getExternalIP());
		
		//curl -X PUT -d "ip=127.0.0.1&port=8080" localhost:8080/peers/1
		TrackerUtils.sendAlive("http://localhost:8080"); 
		
		//Generate Shared File for announcing
		SharedFile sh = SharedFile.makeNewSharedFile(DEFAULT_DOWNLOAD_FOLDER+"/teste.txt");
		File testfile = new File(System.getProperty("user.home")+"/.tracker_files/teste.txt.tracker");
		try{
			BufferedWriter w = new BufferedWriter(new FileWriter(testfile));
			System.out.println(TrackerUtils.save(sh));
			w.write(TrackerUtils.save(sh));
			w.close();
		}catch(IOException e){
			e.printStackTrace();
		}	
		SharedFile sf = TrackerUtils.parse(testfile);

		//curl -X POST -d "name=Name&length=4096&description=Desc&checksum=42&checksum_piece1=42&peer_id=1" localhost:8080/'
        //TrackerUtils.createFileTracker(sh, "http://localhost:8080/", "");
		
        //curl localhost:8080
        TrackerUtils.getFileList("http://localhost:8080");
        
        //curl -X PUT localhost:8080/Name.tracker/pieces/1/peers/2'
		//TrackerUtils.announcePiece("http://localhost:8080", "teste.txt", "1");
		
		//curl localhost:8080/teste.txt.tracker
		//File f = TrackerUtils.retrieveTrackerFile("http://localhost:8080", "teste.txt");

		//BufferedReader br = new BufferedReader(new FileReader(f));
		//String line = "";
		//while((line = br.readLine())!=null){
//			System.out.println(line);
		//}
		System.out.println("done");
	}
	private static void loadProperties() {
		try{
			BufferedReader br = new BufferedReader(new FileReader(DEFAULT_CONFIG_FILE));
			properties.load(br);
			br.close();
		}catch(IOException e){
			//Do nothing for now
		}	
		if(!properties.stringPropertyNames().contains("peer_id")){
			System.out.println("Generating new Peer ID");
			properties.setProperty("peer_id", Utils.getNewPeerID());
		}
		if(!properties.stringPropertyNames().contains("peer_port")){
			properties.setProperty("peer_port", new Integer(DEFAULT_SERVER_PORT).toString());
		}
		if(!properties.stringPropertyNames().contains("share_folder")){
			properties.setProperty("share_folder", DEFAULT_DOWNLOAD_FOLDER);
		}
		File f = new File(properties.getProperty("share_folder"));
		if(!f.exists()){
			System.out.print("Folder "+properties.getProperty("share_folder")+"not found, defaulting to ");
			System.out.println(DEFAULT_DOWNLOAD_FOLDER);
		}
		for(String k: properties.stringPropertyNames()){
			System.out.println(k+" = "+properties.getProperty(k));
		}
	}

}
