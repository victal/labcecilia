package download;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.Callable;

import main.Main;
import utils.TrackerUtils;

public class DownloadWorker implements Callable<Boolean> {

	private SharedFile sf;
	private DownloadPiece piece;
	private int pieceindex;
	private boolean downloaded;
	
	public DownloadWorker(SharedFile sf, DownloadPiece piece, int pieceindex){
		this.sf = sf;
		this.piece = piece;
		this.pieceindex = pieceindex;
		this.downloaded = false;
	}
	
	@Override
	public Boolean call(){
		while(!downloaded && piece.getPeerlist().size()>0){
			Peer p = piece.getPeerlist().get(0);
			downloaded = downloadFrom(p);
			if(!downloaded) piece.getPeerlist().remove(0);
		}
		//Do something after downloaded piece - talk to tracker, p.ex.
		if(downloaded)
			TrackerUtils.announcePiece(Main.properties.getProperty("tracker"),sf.getName(),new Integer(pieceindex).toString());
		return downloaded;

	}
	private boolean downloadFrom(Peer p){
		//Try to download from peer, if it works save file on Main.DEFAULT_DOWNLOAD_FOLDER
		//and return true. if not remove peer from list and return false 
		try {
//			System.out.println(p.getIP()+ ":"+p.getPort() );
			Socket s = new Socket(p.getIP(), p.getPort());
			PrintWriter out = new PrintWriter(s.getOutputStream(),true);
			BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			out.println("Request:"+sf.getName()+":"+pieceindex);
//			System.out.println("Send Request:name:pieceindex");
			String read = in.readLine();
			//Check if not ack
			if(!read.equals("Ack")){}
			out.println("Start");
//			System.out.println("Send Start");
			DataInputStream dis = new DataInputStream(s.getInputStream());
			boolean downloaded = receiveFile(dis);
//			System.out.println("Post ReceiveFile");
			//checksum is looked at in receiveFile
			if(downloaded){
				sf.addPiece(pieceindex);
				out.println("OK");
			}
			else {out.println("Error"); System.out.println("error on piece "+pieceindex);}
			dis.close();
			in.close();
			out.close();
			s.close();
			return downloaded;
		} catch (IOException e) {
			System.out.println("Download of piece "+pieceindex+" from "+p.getIP()+"failed.");
			return false;
		}
	}
	private boolean receiveFile(DataInputStream dis) throws IOException{
		byte[] bytes = new byte[piece.getSize()];
		int off = 0, size = piece.getSize();
		int read = -1;
		while(size>0){
			read = dis.read(bytes, off, size);
			if(read == -1)System.out.println("Something really wrong here");
			size-=read;
			off+=read;
//			System.out.println(size);
		}
		System.out.println("receivefile read "+off+" bytes");
		System.out.println("piecesize = "+piece.getSize());
		if(off != piece.getSize()) return false;
		//For testing
		//String fullpath = DownloaderTestMain.DEFAULT_DOWNLOAD_FOLDER+File.separator + sf.getName()+"."+pieceindex;
		String fullpath = Main.properties.getProperty("share_folder")+File.separator + sf.getName()+"."+pieceindex;
		System.out.println("receivefile fullpath " + fullpath);
		FileOutputStream out = new FileOutputStream(fullpath);
		out.write(bytes);
		out.close();
//		System.out.println(new File(fullpath).exists());
//		System.out.println(sf.checkPart(pieceindex));
		if(sf.checkPart(pieceindex))return true;
		return false;
	}
}
