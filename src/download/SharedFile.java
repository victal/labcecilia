package download;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import main.Main;
import utils.TrackerUtils;
import utils.Utils;

public class SharedFile {
	static private HashMap<String,SharedFile> sharedfiles = null;
	static public int piecesize = 4096;//bytes
	private String name;
	private Integer id;
	private long length; //bytes
	private int numpieces;
	private List<DownloadPiece> pieces;
	private String checksum;
    private String description;

	private String fullpath;


	public SharedFile(long length, String fullpath, String checksum, List<DownloadPiece> pieces){
		String[] path = fullpath.split("\\"+File.separator);
		this.name = path[path.length-1];
		this.length = length;
		this.numpieces = (int)Math.ceil((double)length/piecesize);
		this.fullpath = fullpath;
		this.pieces = pieces;
		this.checksum = checksum;
	}

	//common getters
	public String getName() {
		return name;
	}

	public long getLength() {
		return length;
	}

	public int getNumpieces() {
		return numpieces;
	}

	public boolean hasPiece(Integer piece){
		return pieces.get(piece-1).isOwned();
	}

	public void addPiece(Integer piece){
		pieces.get(piece-1).setOwned(true);
	}
	public int getPiecesize(int piece) {
		if(piece<numpieces) return SharedFile.piecesize;
		else if(piece == numpieces) return (int)(length-(numpieces-1)*piecesize);
		return -1;
	}

	public List<DownloadPiece> getPieces(){
		return pieces;
	}

	//file and part-related methods
	public boolean isWhole(){
		File f = new File(fullpath);
		return f.exists();
	}

	public RandomAccessFile getFile(){
		try{
			RandomAccessFile raf = new RandomAccessFile(fullpath,"r");
			return raf;
		}catch(FileNotFoundException e){
			e.printStackTrace();
			return null;
		}
	}

	public RandomAccessFile getPartFile(Integer part){
		String path = fullpath+"."+part.toString();
		try{
			RandomAccessFile raf = new RandomAccessFile(path,"r");
			return raf;
		}catch(FileNotFoundException e){
			return null;
		}

	}

	public boolean checkPart(Integer part) {
	//Used if there is a partial file
		System.out.println("Checksum for part "+part);
		if(this.isWhole()){
			int off = 0;
			for(int i = 1;i<part;i++){
				off+=pieces.get(i-1).getSize();
			}
			DownloadPiece p = pieces.get(part-1);
			int len = p.getSize();
			String md5 = Utils.calculatePartialMD5(this.fullpath, off, len);
			System.out.println("calculado "+md5);
			System.out.println("original "+p.getChecksum());
			return p.getChecksum().equals(md5);
		}
		else{
			String path = fullpath+"."+part.toString();
			System.out.println(path);
			String md5 = Utils.calculateMD5Sum(path);
			DownloadPiece p = pieces.get(part-1);
			String checksum = p.getChecksum();
			System.out.println("calculado "+md5);
			System.out.println("original "+p.getChecksum());
			return(checksum.equals(md5));
		}
	}

	//checksum-related methods
	public String calculateMD5Sum(){
		return Utils.calculateMD5Sum(fullpath);
			}

	public String getOriginalMD5(){
		return checksum;
	}

	//static methods
	public static SharedFile getFile(String filename) {
		return sharedfiles.get(filename);
	}
	public static void loadSharedFiles(){

		if(sharedfiles!=null)return;
		sharedfiles = new HashMap<String,SharedFile>();
		String infodir = Main.properties.getProperty("trackerfile_folder");
		File dir = new File(infodir);
		if(!dir.exists() || !dir.isDirectory()) return;
		else{
			FileFilter filter = new FileFilter() {
				public boolean accept(File pathname) {
					if(pathname.getName().endsWith(".tracker"))
						return true;
					return false;
				}
			};
			for(File tracker: dir.listFiles(filter)){
				SharedFile sf = TrackerUtils.parse(tracker);
				for(int i = 0;i<sf.getNumpieces();i++){
					sf.getPieces().get(i).setOwned(true);
				}
				System.out.println("loaded "+sf.getName());
				sharedfiles.put(sf.getName(),sf);
			}
		}
	}

	public void makeFullFile(){
		try{
			FileOutputStream fos=new FileOutputStream(fullpath);
			FileInputStream fis;
			byte[] piece = new byte[4096];
			for(Integer i = 1; i<= numpieces; i++){
				fis = new FileInputStream(fullpath+"."+i);
				fis.read(piece, 0, pieces.get(i-1).getSize());
				fos.write(piece, 0, pieces.get(i-1).getSize());
				fis.close();
				File f = new File(fullpath+"."+i);
				if(f.exists()){
//					System.out.println(f.getAbsolutePath());
					f.delete();
				}
			}
			fos.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}


	public void setFullPath(String path){
		this.fullpath = path;
	}
	public String getFullPath(){
		return this.fullpath;
	}
	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static SharedFile makeNewSharedFile(String fullpath) {
		File f = new File(fullpath);
		if(!f.exists()){
			return null;
		}
		//For testing
		//Peer p = new Peer(UploaderTestMain.peer_id, Utils.getLocalIP(), UploaderTestMain.server_port);
		int server_port = Integer.parseInt(Main.properties.getProperty("peer_port"));
		Peer p = new Peer(Main.properties.getProperty("peer_id"), Utils.getLocalIP(), server_port);
		List<Peer> pl = new ArrayList<Peer>();
		pl.add(p);

		long length = f.length();
		long remaining = f.length();
		int start = 0;
		List<DownloadPiece> dpl = new ArrayList<DownloadPiece>();
		int size;
		int i = 1;
		while(remaining >0){
			size = piecesize;
			if(remaining<piecesize)size = (int)remaining;
			String checksum = Utils.calculatePartialMD5(fullpath, start, size);
			DownloadPiece dp= new DownloadPiece(i,size,checksum,pl);
			dp.setOwned(true);
			dpl.add(dp);
			remaining -= size;
			start+=size;
			i++;
		}
		//System.out.println("List order on makefullfile");
		//for(i = 0;i<dpl.size();i++){
		//	System.out.println("Piece index "+i+" is "+dpl.get(i).getID());
		//}
		String file_checksum = Utils.calculateMD5Sum(fullpath);
		return new SharedFile(length,fullpath,file_checksum,dpl);
	}

}
