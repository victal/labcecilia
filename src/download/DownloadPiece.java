package download;

import java.util.List;

public class DownloadPiece {
        private Integer id;
	private int size;
	private String check_sum;
	private List<Peer> peerList;
	private boolean owned = false;
	public DownloadPiece(int size, String sum, List<Peer> list){
		this.size = size;
		this.check_sum = sum;
		this.peerList = list;
		this.owned = false;
	}
	public DownloadPiece(int id,int size, String sum, List<Peer> list){
		this.id = id;
		this.size = size;
		this.check_sum = sum;
		this.peerList = list;
		this.owned = false;
	}
	public int getSize() {
		return size;
	}
	public String getChecksum() {
		return check_sum;
	}
	public List<Peer> getPeerlist() {
		return peerList;
	}
	public boolean isOwned(){
		return owned;
	}
	public void setOwned(boolean b){
		this.owned = b;
	}
	public void setChecksum(String checksum) {
		this.check_sum = checksum;

	}
	public int getID(){
		return id;
	}

}
