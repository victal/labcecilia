package download;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Downloader implements Runnable{
	SharedFile sf;
	List<DownloadPiece> todownload;
	List<Thread> workerThreads;
	List<DownloadWorker> workers;
	
	public Downloader(SharedFile sf){
		this.sf = sf;
		todownload = new ArrayList<DownloadPiece>();
		for(DownloadPiece p: sf.getPieces()){
			if(!p.isOwned())
				todownload.add(p);
		}
		workers = new ArrayList<DownloadWorker>();
	}
	
	public void run(){
		ExecutorService exsvc = Executors.newCachedThreadPool(); 
		for(DownloadPiece p: todownload){
			DownloadWorker worker = new DownloadWorker(sf,p,todownload.indexOf(p)+1);
			workers.add(worker);
		}
		try{
			List<Future<Boolean>>results = exsvc.invokeAll(workers);
			for(Future<Boolean> b: results){
				if(!b.get()){
					System.out.println("piece "+results.indexOf(b)+" not downloaded.");
					return;
				}
			}
			sf.makeFullFile();
			
		}
		catch(InterruptedException | ExecutionException e){
			e.printStackTrace();
		}
		
	}
}
