package upload;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import main.Main;

public class UploadServer implements Runnable{

	
	private int port;
	private boolean running = true;
	private List<UploadWorker> workers = new ArrayList<UploadWorker>();
	public UploadServer(){
		this.port = Integer.parseInt(Main.properties.getProperty("peer_port"));
	}
	public UploadServer(int port){
		this.port = port;
	}
	@Override
	public void run() {
		try {
			ServerSocket ss = new ServerSocket(port);
			ss.setSoTimeout(0);
			System.out.println("Starting server socket on port "+port);
//			System.out.println(ss.getInetAddress().getHostAddress());
			while(running){
				Socket uploader = ss.accept();
				UploadWorker uw = new UploadWorker(uploader);
				workers.add(uw);
				new Thread(uw).start();
				System.out.println("connected from "+uploader.getInetAddress().getHostName());
			}
			
			for(UploadWorker uw:workers){
				if(!uw.closed()){
					uw.close();
				}
			}
			ss.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	public void stop(){
		System.out.println("Stopping upload server");
		this.running = false;
	}

}
