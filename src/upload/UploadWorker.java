package upload;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.net.Socket;

import main.Main;
import utils.TrackerUtils;
import download.SharedFile;

public class UploadWorker implements Runnable{

	private Socket s;
	private boolean open = true;
	private PrintWriter out;
	private BufferedReader in;
	public UploadWorker(Socket s){
		this.s=s;
		try{
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			out = new PrintWriter(s.getOutputStream(),true);
		}
		catch(IOException e){
			e.printStackTrace();
		}	
	}
	
	@Override
	public void run() {
		String received="",filename = "",part = "";
		try {
			received = in.readLine();
			System.out.println(received);
			String[] msg = received.split("[:]");//"Dentro da lista todo mundo é normal"
			if(msg.length!=3){System.out.println("malformed message");}
			else{
				filename = msg[1];
				part = msg[2];
			}
			out.println("Ack");
			received = in.readLine();
			if(received.equals("Start")){
				this.sendFile(filename,part);
				received = in.readLine();
				if(received.equals("Error")){
					checkFile(filename,part);				
				}
			}
		} catch (IOException e) {	
			e.printStackTrace();
		}
		while(open){}	
	}
	private boolean checkFile(String filename, String part) {
		SharedFile s = SharedFile.getFile(filename);
		boolean correct = s.checkPart(Integer.parseInt(part));
		if(!correct){
			TrackerUtils.deleteFromPeerList(Main.properties.getProperty("tracker"),filename,Integer.parseInt(part));
		}
		return correct;
		 
	}
	
	//writes zeroes if getFile or getPartFile ==null
	//TODO: something useful on nulls
	private void sendFile(String filename, String part) {
		try{
		int p = Integer.parseInt(part);
		DataOutputStream dos = new DataOutputStream(s.getOutputStream());
		dos.flush();
		SharedFile sfile = SharedFile.getFile(filename);
		byte[] towrite = new byte[SharedFile.piecesize];
		for(int i = 0;i<towrite.length;i++){
			towrite[i] = (byte)0;
		}
		if(sfile.isWhole()){
			RandomAccessFile file = sfile.getFile();
			if(file!=null){
				System.out.println((p-1));
				sfile.checkPart(p);
				file.seek((p-1)*SharedFile.piecesize);
				file.read(towrite,0,SharedFile.piecesize);
			}
			dos.write(towrite,0,SharedFile.piecesize);
		}
		else{
			RandomAccessFile file = sfile.getPartFile(p);
			if(file!=null)
				file.read(towrite,0,SharedFile.piecesize);
			dos.write(towrite,0,SharedFile.piecesize);
		}
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	public boolean closed(){
		return !open;
	}
	public void close(){
		open = false;
		try {
			in.close();
			out.close();
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
