package utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;

import main.Main;

public class Utils {

	static final byte[] HEX_CHAR_TABLE = {
	    (byte)'0', (byte)'1', (byte)'2', (byte)'3',
	    (byte)'4', (byte)'5', (byte)'6', (byte)'7',
	    (byte)'8', (byte)'9', (byte)'a', (byte)'b',
	    (byte)'c', (byte)'d', (byte)'e', (byte)'f'
	};    
	
	
	public static String getHexString(byte[] raw){
	    byte[] hex = new byte[2 * raw.length];
	    int index = 0;

	    for (byte b : raw) {
	      int v = b & 0xFF;
	      hex[index++] = HEX_CHAR_TABLE[v >>> 4];
	      hex[index++] = HEX_CHAR_TABLE[v & 0xF];
	    }
	    try {
			return new String(hex, "ASCII");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getNewPeerID(){
//		File f = new File(configfile);
//		if(f.exists()){
//			try{
//				BufferedReader reader = new BufferedReader(new FileReader(f));
//				String idstr = reader.readLine();
//				reader.close();
//				if(idstr.startsWith("peer_id") && idstr.split("=").length>1){
//					return idstr.split("=")[1];
//				}
//			}catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
		String externalIP = getLocalIP(); 
		if(externalIP == null)
			externalIP = Utils.getExternalIP();
		int port = Integer.parseInt(Main.properties.getProperty("peer_port"));
		Integer id = externalIP.hashCode() + 23 * port;
//		if(!f.exists()){
//			try{
//				f = new File(configfile);
//				BufferedWriter bw = new BufferedWriter(new FileWriter(f));
//				bw.write("peer_id="+id);
//				bw.close();
//			} catch(IOException e){
//				e.printStackTrace();
//			}
//		}
		return id.toString();
		
	}
	
	public static String getLocalIP(){
		Enumeration<NetworkInterface> ifaces;
		try {
			ifaces = NetworkInterface.getNetworkInterfaces();
		
		while(ifaces.hasMoreElements()){
			NetworkInterface iface = (NetworkInterface)ifaces.nextElement();
			Enumeration<InetAddress> ip = iface.getInetAddresses();
			while(iface.isUp() && !iface.isPointToPoint() && ip.hasMoreElements()){
				InetAddress addr = ip.nextElement();
				if(!addr.isLinkLocalAddress() && !addr.isLoopbackAddress()){
					return addr.getHostAddress();
				}
			}
		}
		} catch (SocketException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}
	//Uses whatsmyip.com services
	public static String getExternalIP(){
		//TODO: Use fallbacks
		//api-ams01.exip.org
		//api-nyc01.exip.org
		//whatismyip.com
		URL whatismyip;
		try {
			whatismyip = new URL("http://api-sth01.exip.org/?call=ip");
			BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
			String ip = in.readLine();
			return ip;
		} catch (IOException e) {
			e.printStackTrace();
			return "127.0.0.1";
		}
	}
	
	public static String calculateMD5Sum(String filepath){
		MessageDigest md = null;
		InputStream fis;
		byte[] buffer = new byte[1024];
		try {
			fis = new FileInputStream(filepath);
			md = MessageDigest.getInstance("MD5");
			int numRead;
		    do {
		    	numRead = fis.read(buffer);
		    	if (numRead > 0) {
		    		md.update(buffer, 0, numRead);
		    	}
		    } while (numRead != -1);
	        fis.close();
		
		} catch (IOException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		if(md!=null){
			return Utils.getHexString(md.digest());}
		return null;

	}

	public static String calculatePartialMD5(String fullpath, int off,int len){
		MessageDigest md = null;
		RandomAccessFile raf;
		
		byte[] buffer = new byte[len];
		try {
			raf = new RandomAccessFile(fullpath, "r");
			md = MessageDigest.getInstance("MD5");
			raf.seek(off);
			raf.read(buffer, 0, len);
			raf.close();
		    return Utils.getHexString(md.digest(buffer));
		} catch (IOException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}


}

