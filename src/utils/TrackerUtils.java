package utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import main.Main;

import com.thoughtworks.xstream.XStream;

import download.DownloadPiece;
import download.Peer;
import download.SharedFile;

public class TrackerUtils {

	public static SharedFile parse(File trackerFile){
		XStream xstream = new XStream();
		xstream.setMode(XStream.NO_REFERENCES);
		xstream.alias("peer",Peer.class);
		xstream.alias("file",SharedFile.class);
		xstream.alias("piece", DownloadPiece.class);
		SharedFile sh = (SharedFile)xstream.fromXML(trackerFile);
		if(sh.getName() == null){
			String name = trackerFile.getName().replaceAll(".tracker$", "");
			sh.setName(name);
		}
		if(sh.getFullPath()==null){
			String name = trackerFile.getName().replaceAll(".tracker$", "");
			//For Testing
			//String path = UploaderTestMain.DEFAULT_DOWNLOAD_FOLDER+File.separator+name;
			String path = Main.DEFAULT_DOWNLOAD_FOLDER+File.separator+name;
			sh.setFullPath(path);
		}
		return sh;
	}

	public static String save(SharedFile sf){
		XStream xstream = new XStream();
		xstream.setMode(XStream.NO_REFERENCES);
		xstream.alias("peer",Peer.class);
		xstream.alias("file",SharedFile.class);
		xstream.alias("piece", DownloadPiece.class);
		xstream.omitField(DownloadPiece.class, "owned");
		xstream.omitField(SharedFile.class, "name");
		xstream.omitField(SharedFile.class, "fullpath");
		String xml = xstream.toXML(sf);
		return xml;
	}
	//  curl -X DELETE /resource.tracker/pieces/2/peers/123
	public static void deleteFromPeerList(String tracker, String filename, Integer part){
		String url = tracker+"/"+filename+".tracker/pieces/"+part+"/peers/"+Main.properties.getProperty("peer_id");

		try {
			HttpURLConnection connection = (HttpURLConnection)new URL(url).openConnection();
			//connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded" );
			connection.setDoOutput(true);
			connection.setRequestMethod("DELETE");
			connection.disconnect();
			int i = connection.getResponseCode();
			System.out.println("Announced deletion of "+filename+"."+part.toString()+"to tracker");
			System.out.println("Response Code "+i);
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	//TODO: what if trackerfile is a full path?
	//GET curl localhost:8000/resourcename.tracker
	public static File retrieveTrackerFile(String tracker, String trackerFile) {
		String url = tracker+"/"+trackerFile;
		if(!trackerFile.endsWith(".tracker"))url+=".tracker";
		try{
			HttpURLConnection connection = (HttpURLConnection)new URL(url).openConnection();
			int code = connection.getResponseCode();
			if(code == 404) return null;
			String filename = Main.DEFAULT_DOWNLOAD_FOLDER+File.separator+trackerFile;
			if(!filename.endsWith(".tracker"))filename+=".tracker";
			File f = new File(filename);
			BufferedWriter bw = new BufferedWriter(new FileWriter(f));
			BufferedReader r = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputLine;
	        while ((inputLine = r.readLine()) != null)
	            bw.write(inputLine);
	        r.close();
	        bw.close();
	        System.out.println("Retrieved .tracker file from tracker: "+trackerFile);
			return f;
		}catch(IOException e){
			e.printStackTrace();
			return null;
		}
	}

	//  curl -X PUT  /resource.tracker/pieces/2/peers/123
	public static void announcePiece(String tracker, String filename, String part){
		String url = tracker+"/"+filename+".tracker/pieces/"+part+"/peers/"+Main.properties.getProperty("peer_id");
		try {
			HttpURLConnection connection = (HttpURLConnection)new URL(url).openConnection();
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestMethod("PUT");
			System.out.println("Sent Peer information to tracker");
			connection.disconnect();
			int i = connection.getResponseCode();
			System.out.println("Annnounced piece "+filename+"."+part+" to tracker.");
			System.out.println("Response Code "+i);
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	//PUT implemented as per http://stackoverflow.com/questions/1051004 answer
	public static void sendAlive(String tracker){
		String url = tracker+"/peers/"+Main.properties.getProperty("peer_id");
		OutputStreamWriter output = null;
		int port = Integer.parseInt(Main.properties.getProperty("peer_port"));
		String query = "ip="+Utils.getLocalIP()+"&port="+port;
		try {
			HttpURLConnection connection = (HttpURLConnection)new URL(url).openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestMethod("PUT");
			output = new OutputStreamWriter(connection.getOutputStream());
			output.write(query);
			output.close();

			connection.disconnect();
			int i = connection.getResponseCode();
			System.out.println("Sent Peer information to tracker");
			System.out.println("Response Code "+i);

		}catch(IOException e){
			System.out.println("Could not connect to tracker.");
			//e.printStackTrace();
		} finally {
			if (output != null) try { output.close(); } catch (IOException logOrIgnore) {
				logOrIgnore.printStackTrace();
			}
		}

	}
	//TODO: Use description
	//curl -X POST -d "name=Name&length=4096&description=Desc&checksum=42&checksum_piece1=42&peer_id=1" localhost:8080/'
	public static void createFileTracker(SharedFile done, String tracker, String description) {
		if(description == null || description.length()==0 ){
			description = "Description";
		}
		try {
			String query = "name="+done.getName()+"&length="+new Long(done.getLength()).toString()+"&description="+description+
						   "&checksum="+done.getOriginalMD5();
			 //System.out.println("Piece order on createfiletracker");
			//for(int i = 0;i<done.getPieces().size();i++){
			//	System.out.println("Piece index "+i+" is "+done.getPieces().get(i).getID());
			//}
			for(int i = 0;i<done.getPieces().size();i++){
				query+="&checksum_piece"+(i+1)+"="+ done.getPieces().get(i).getChecksum();
			}
			query+="&peer_id="+Main.properties.getProperty("peer_id");
			HttpURLConnection connection = (HttpURLConnection)new URL(tracker).openConnection();
			connection.setDoOutput(true);// Triggers POST.
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			OutputStreamWriter output = null;
			try {
			     output = new OutputStreamWriter(connection.getOutputStream());
			     output.write(query);
			     System.out.println("File information sent to tracker.");
			} finally {
			     if (output != null) try { output.close(); } catch (IOException logOrIgnore) {
			    	 System.out.println("Could not post info to tracker.");
			     }
			}
			System.out.println("Response Code " + connection.getResponseCode());
		} catch (IOException e) {
			System.out.println("Could not post info to tracker.");
			//e.printStackTrace();
		}

	}

	public static void getFileList(String tracker) {
		try{
		URLConnection conn = new URL(tracker).openConnection();
		conn.setRequestProperty("Accept-Charset", "ASCII");
		InputStream response = conn.getInputStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(response));
		String line = "none";
		while((line = br.readLine())!=null){
			System.out.println(line);
		}
		}catch(IOException e){
			e.printStackTrace();
		}
	}


}
